import '../css/app.css';
// import { randomUserMock, additionalUsers as _additionalUsers } from './FE4U-Lab2-mock.js';

// const randomUsers = randomUserMock;
// const additionalUsers = _additionalUsers;

/** ******** Your code here! *********** */
let teacherDisplay;
let favoriteDisplay;
let teacherStats;

/* ****** Task 1 ****** */
function calculateAge(birthday) {
    const birthDate = new Date(birthday);
    const currentDate = new Date();
    let age = currentDate.getFullYear() - birthDate.getFullYear();

    if (currentDate < new Date(currentDate.getFullYear(), birthDate.getMonth(), birthDate.getDate())) {
        age--;
    }

    return age;
}

const courses = [
    "Mathematics",
    "Physics",
    "English",
    "Computer Science",
    "Dancing",
    "Chess",
    "Biology",
    "Chemistry",
    "Law",
    "Art",
    "Medicine",
    "Statistics"
];

function formatUser(user) {
    const age = calculateAge(user.b_day);

    const formattedUser = {
        "id": user.id || '',
        "gender": user.gender || '',
        "title": user.title || (user.name ? user.name.title : ''),
        "full_name": user.full_name || (user.name ? `${user.name.first} ${user.name.last}` : ''),
        "city": user.city || (user.location ? user.location.city : ''),
        "state": user.state || (user.location ? user.location.state : ''),
        "country": user.country || (user.location ? user.location.country : ''),
        "postcode": user.postcode || (user.location ? user.location.postcode : ''),
        "coordinates": user.coordinates || (user.location ? user.location.coordinates : {}),
        "timezone": user.timezone || (user.location ? user.location.timezone : {}),
        "email": user.email || '',
        "b_date": user.b_day || '',
        "age": age || (user.dob && user.dob.age),
        "phone": user.phone || '',
        "favorite": user.favorite || false,
        "course": user.course || courses[Math.floor(Math.random() * courses.length)],
        "bg_color": user.bg_color || 'blue',
        "note": user.note || 'Is a person.',
    };

    if (user.picture_large) {
        formattedUser.picture_large = user.picture_large;
    } else if (user.picture && user.picture.large) {
        formattedUser.picture_large = user.picture.large;
    }

    if (user.picture_thumbnail) {
        formattedUser.picture_thumbnail = user.picture_thumbnail;
    } else if (user.picture && user.picture.thumbnail) {
        formattedUser.picture_thumbnail = user.picture.thumbnail;
    }

    return formattedUser;
}

function mergeAndFormat(userArray1, userArray2 = []) {
    const mergedUsers = [...userArray1, ...userArray2];
    const formattedUsers = mergedUsers.map(formatUser);

    const uniqueFormattedUsers = [];
    const seenFullNames = new Set();

    for (const user of formattedUsers) {
        if (!seenFullNames.has(user.full_name)) {
            uniqueFormattedUsers.push(user);
            seenFullNames.add(user.full_name);
        }
    }

    return uniqueFormattedUsers;
}

//----------------------------Fetching data----------------------------
let randomUsers;

fetch('https://randomuser.me/api/?results=50')
    .then(response => response.json())
    .then(data => {
        const randomUsersFromAPI = data.results;

        randomUsers = mergeAndFormat(randomUsersFromAPI, randomUsers);
    })
    .catch(error => {
        console.error('Something went wrong: ', error);
    });

/* ****** Task 2 ****** */
function startsWithCapital(str) {
    return typeof str === 'string'
        && str.length > 0
        && str.charAt(0).toLocaleUpperCase() === str.charAt(0);
}

function isPhoneNumValid(phone, country) {
    const phoneNumCheckUA = /^(\+38\s?)?((\(0[1-9]{2}\))|(0[1-9]{2}))(\s|-)?[0-9]{3}(\s|-)?[0-9]{2}(\s|-)?[0-9]{2}$/g;
    const phoneNumCheck = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/g;

    if (country === 'Ukraine')
        return phoneNumCheckUA.test(phone);

    return phoneNumCheck.test(phone);
}

function isEmailValid(email) {
    const emailCheck = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;

    return emailCheck.test(email);
}

function validate(user) {
    return startsWithCapital(user.full_name) && startsWithCapital(user.country) && startsWithCapital(user.city)
        && Number.isInteger(user.age)
        && (user.gender === 'male' || user.gender === 'female')
        && isEmailValid(user.email);
    //&& isPhoneNumValid(user.phone);
}

/* ****** Task 3 ****** */
function filterArray(array, age, country, gender, favorite) {
    return array.filter((item) =>
        (age === null || age === '' || item.age === age || checkAgeRange(item.age, age))
        && (country === null || country === 'all' || item.country === country)
        && (gender === null || gender === 'all' || item.gender === gender)
        && (favorite === null || favorite === '' || favorite === false || item.favorite === favorite));
}

function checkAgeRange(age, ageRange) {
    if (ageRange === "all") {
        return true;
    }

    if (ageRange === "61+") {
        return age >= 61;
    }

    const [min, max] = ageRange.split('-').map(Number);
    return age >= min && age <= max;
}

/* ****** Task 4 ****** */
function sortedArray(array, field, isAscending = true) {
    return array.sort((a, b) => {
        if (a[field] < b[field]) return isAscending ? -1 : 1;
        if (a[field] > b[field]) return isAscending ? 1 : -1;
        return 0;
    });
}

/* ****** Task 5 ****** */
function findTeacher(array, parameter, searchPrompt) {
    const search = searchPrompt.toLowerCase();

    if (parameter === 'text') {
        return array.filter((obj) =>
            (obj.full_name && obj.full_name.toLowerCase().includes(search)) ||
            (obj.note && obj.note.toLowerCase().includes(search))
        );
    } else if (parameter === 'num') {
        return array.filter((obj) =>
            (obj.age && obj.age.toString().includes(searchPrompt))
        );
    }

    return [];
}

// LAB 3

//----------------------------------Populate site with array data----------------------------------
function createTeacherCard(user) {
    const initials = user.full_name
        .split(' ')
        .map((name) => name.charAt(0).toLocaleUpperCase())
        .join('');

    const teacherPic = user.picture_large || user.picture_thumbnail;
    const star = user.favorite ? '<span class="star favorite">&#9733;</span>' : '';

    return `
        <div class="teacher-card">
            <div class="teacher-icon"><img src="${teacherPic}" alt="${initials}"></div>
            <div class="teacher-info">
                <h4>${star}${user.full_name}<br></h4>
                <h5>${user.course}</h5>
                <h6>${user.country}</h6>
            </div>
        </div>
    `;
}

function createTeacherList(teacherArray) {
    teacherDisplay.innerHTML = teacherArray.map(createTeacherCard).join('');
}

function createFavoriteList(teacherArray) {
    favoriteDisplay.innerHTML = teacherArray.map(createTeacherCard).join('');
}


//----------------------------------pain (Add teacher info pop-ups)----------------------------------
let infoPopup;
let closeBtn;

infoPopup = document.createElement('dialog');
infoPopup.classList.add('pop-up');

document.body.appendChild(infoPopup);

function openTeacherInfoPopup(index, users) {
    const teacherData = users[index];
    const dialogContent = teacherInfoPopup(teacherData);
    infoPopup.innerHTML = dialogContent;
    infoPopup.showModal();

    closeBtn = document.querySelector('.pop-up-closeBtn');
    closeBtn.addEventListener('click', closeInfoPopup, false);
}

function teacherInfoPopup(teacher) {
    let markup;
    let teacherLocation = teacher.city + (", " + teacher.state + ", " || ", ") + teacher.country;
    let teacherAgeGender = teacher.age + ", " + teacher.gender;
    let teacherFavorite = teacher.favorite;

    const popUpHeader = document.createElement('div');
    popUpHeader.classList.add('pop-up-header');

    const closeBtn = document.createElement('button');
    closeBtn.classList.add('pop-up-closeBtn');
    closeBtn.textContent = 'X';

    popUpHeader.appendChild(closeBtn);
    infoPopup.appendChild(popUpHeader);

    markup = `
    <div class="pop-up-container">
        <div class="pop-up-block pop-up-block-large">
            ${popUpHeader.outerHTML}
            <div class="info-block">
                <div class="info-main">
                    <img class="info-main-img" alt="teacher's photo" src="${teacher.picture_large}">
                    <div class="info-main-text">
                        <h2 class="info-main-name">${teacher.full_name}</h2>
                        <h3 class="info-main-subject">${teacher.course}</h3>
                        <p class="info-main-p">${teacherLocation}</p>
                        <p class="info-main-p">${teacherAgeGender}</p>
                        <p class="info-main-p"><a href="mailto:${teacher.email}">${teacher.email}</a></p>
                        <p class="info-main-p">${teacher.phone}</p>
                    </div>
                    <span class="star ${teacherFavorite ? 'favorite' : ''}">${teacherFavorite ? '★' : '☆'}</span>
                </div>
                <p class="info-main-p">
                    ${teacher.note}
                </p>
                <a class="info-block-link" href="/">toggle map</a>
            </div>
        </div>
    </div>
    `;

    return markup;
}

function closeInfoPopup() {
    infoPopup.close();
    closeBtn.removeEventListener('click', closeInfoPopup, false);
}

function toggleFavorite(starElement) {
    const teacherNameElement = starElement.previousElementSibling.querySelector('.info-main-name');
    const teacherData = randomUsers.find((teacher) => teacher.full_name === teacherNameElement.textContent);

    if (!teacherData) {
        console.error("Teacher data not found.");
        return;
    }

    teacherData.favorite = !teacherData.favorite;

    if (teacherData.favorite) {
        starElement.classList.add('favorite');
        starElement.textContent = '★';
    } else {
        starElement.classList.remove('favorite');
        starElement.textContent = '☆';
    }

    localStorage.setItem(`favorite_${teacherData.full_name}`, teacherData.favorite);
}

document.addEventListener('click', function (event) {
    if (event.target.classList.contains('star')) {
        toggleFavorite(event.target);
    }
});

// Filtering
const selectedAge = document.getElementById('age-select');
const selectedRegion = document.getElementById('region-select');
const selectedSex = document.getElementById('sex-select');
const selectedOnlyFaves = document.getElementById('faves-only');

function getUniqueCountries(users) {
    const countries = new Set();
    users.forEach(user => countries.add(user.country));
    return Array.from(countries);
}

function updateFilterOptions(users) {
    const uniqueCountries = getUniqueCountries(users);

    selectedRegion.innerHTML = '';

    const allOption = document.createElement('option');
    allOption.value = 'all';
    allOption.textContent = 'All';
    selectedRegion.appendChild(allOption);

    uniqueCountries.forEach(country => {
        const option = document.createElement('option');
        option.value = country;
        option.textContent = country;
        selectedRegion.appendChild(option);
    });
}

function filter() {
    const ageVal = selectedAge.value;
    const regVal = selectedRegion.value;
    const sexVal = selectedSex.value;
    const favVal = selectedOnlyFaves.checked;

    const filteredUsers = filterArray(randomUsers, ageVal, regVal, sexVal, favVal);
    createTeacherList(filteredUsers);
    teacherIcons = document.querySelectorAll('.teacher-icon');
    teacherIcons.forEach((icon, index) => {
        icon.addEventListener('click', () => {
            openTeacherInfoPopup(index, filteredUsers);
        });
    });
}

selectedAge.addEventListener('change', filter);
selectedRegion.addEventListener('change', filter);
selectedSex.addEventListener('change', filter);
selectedOnlyFaves.addEventListener('change', filter);


//----------------------------------------------stats table thinb----------------------------------------------
let tableTeachers = randomUsers;
function writeStatsRow(teacher) {
    const { full_name, course, age, gender, country } = teacher;

    return `
        <tr>
            <td>${full_name}</td>
            <td>${course}</td>
            <td>${age}</td>
            <td>${gender}</td>
            <td>${country}</td>
        </tr>
    `;
}

function createStatsTable(teacherArray) {
    let stats = '';
    teacherArray.forEach(user => stats += writeStatsRow(user));
    teacherStats.innerHTML = stats;
}

const teachersPerPage = 10;
let currentPage = 1;
const statsTable = document.querySelector('.stats-table');

function renderTeachersInTable(teachers, page) {
    const startIndex = (page - 1) * teachersPerPage;
    const endIndex = startIndex + teachersPerPage;
    const teachersToDisplay = teachers.slice(startIndex, endIndex);

    statsTable.innerHTML = '';

    for (const teacher of teachersToDisplay) {
        const row = writeStatsRow(teacher);
        statsTable.innerHTML += row;
    }
}

const pageSelector = document.querySelector('.page-selector');

function updatePaginationButtons() {
    const totalPages = Math.ceil(tableTeachers.length / teachersPerPage);

    pageSelector.innerHTML = '';

    for (let page = 1; page <= totalPages; page++) {
        const button = document.createElement('button');
        button.classList.add('pagination-button');
        button.innerText = page;

        button.addEventListener('click', () => {
            renderTeachersInTable(tableTeachers, page);
        });

        pageSelector.appendChild(button);
    }
}

const headerCells = document.querySelectorAll('th[data-sort]');
let currentSortColumn = '';
let isAscending = true;

function sortTeachersByColumn(column) {
    if (currentSortColumn === column) {
        isAscending = !isAscending;
    } else {
        currentSortColumn = column;
        isAscending = true;
    }

    sortedArray(tableTeachers, column, isAscending);

    renderTeachersInTable(tableTeachers, currentPage);
}

headerCells.forEach(cell => {
    cell.addEventListener('click', () => {
        const column = cell.getAttribute('data-sort');
        sortTeachersByColumn(column);
    });
});


//----------------------------------------------search form----------------------------------------------
const searchForm = document.querySelector('.search');
const searchInput = document.getElementById('search-input');

searchForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchValue = searchInput.value.trim();

    let searchResults;

    if (searchValue) {
        let searchType;
        if (!isNaN(searchValue)) {
            searchType = 'num';
        } else {
            searchType = 'text';
        }

        searchResults = findTeacher(randomUsers, searchType, searchValue);
    } else {
        searchResults = randomUsers;
    }

    createTeacherList(searchResults);
    teacherIcons = document.querySelectorAll('.teacher-icon');
    teacherIcons.forEach((icon, index) => {
        icon.addEventListener('click', () => {
            openTeacherInfoPopup(index, searchResults);
        });
    });
});


//----------------------------------------------add teacher----------------------------------------------
let addTeacherDialog;
let closeAddBtn;

addTeacherDialog = document.createElement('dialog');
addTeacherDialog.classList.add('pop-up');

document.body.appendChild(addTeacherDialog);

const addTeacherButtons = document.querySelectorAll(".add-teacher-button");
addTeacherButtons.forEach((button) => {
    button.addEventListener("click", openAddTeacherDialog);
});

function openAddTeacherDialog() {
    const dialogContent = createAddTeacherDialog();
    addTeacherDialog.innerHTML = dialogContent;
    addTeacherDialog.showModal();

    const addTeacherBtn = document.querySelector(".teacher-form-button");
    addTeacherBtn.addEventListener('click', function () {
        addTeacher();
    });

    closeAddBtn = document.querySelector(".add-pop-up-closeBtn");
    closeAddBtn.addEventListener('click', closeAddTeacherDialog, false);
}

function createAddTeacherDialog() {
    let markup;

    const addTeacherHeader = document.createElement('div');
    addTeacherHeader.classList.add('pop-up-header');

    const closeAddBtn = document.createElement('button');
    closeAddBtn.classList.add('add-pop-up-closeBtn');
    closeAddBtn.textContent = 'X';

    const formTitle = document.createElement('h3');
    formTitle.textContent = "Add Teacher";
    formTitle.classList.add('pop-up-title');

    addTeacherHeader.appendChild(formTitle);
    addTeacherHeader.appendChild(closeAddBtn);
    addTeacherDialog.appendChild(addTeacherHeader);

    markup = `
    <div class="pop-up-container">
        <div class="pop-up-block pop-up-block-large">
            ${addTeacherHeader.outerHTML}
            <form class="teacher-form">
                <div class="teacher-form-item">
                    <label class="teacher-form-label" for="name">Name</label>
                    <input class="teacher-form-input" id="name" placeholder="Enter name">
                </div>
                <div class="teacher-form-item">
                    <label class="teacher-form-label" for="speciality">Speciality</label>
                    <select class="teacher-form-input" id="speciality">
                        <option class="teacher-form-input" value="Mathematics">Mathematics</option>
                        <option class="teacher-form-input" value="Chess">Chess</option>
                        <option class="teacher-form-input" value="Vocal">Vocal</option>
                    </select>
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize">
                    <label class="teacher-form-label" for="country">Country</label>
                    <select class="teacher-form-input" id="country">
                        <option class="teacher-form-input" value="Ukraine">Ukraine</option>
                        <option class="teacher-form-input" value="USA">USA</option>
                    </select>
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize">
                    <label class="teacher-form-label" for="city">City</label>
                    <input class="teacher-form-input" id="city">
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize">
                    <label class="teacher-form-label" for="email">Email</label>
                    <input class="teacher-form-input" type="email" id="email">
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize">
                    <label class="teacher-form-label" for="phone">Phone</label>
                    <input class="teacher-form-input"  id="phone">
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize teacher-form-item_alone">
                    <label class="teacher-form-label" for="date">Date of birth</label>
                    <input class="teacher-form-input" type="date" id="date">
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize teacher-form-item_alone">
                    <label class="teacher-form-label label-for-inline">Sex</label>
                    <div class="teacher-form-checkbox">
                        <label class="teacher-form-label" for="male">Male</label>
                        <input class="teacher-form-input" type="radio" id="male" name="sex">
                    </div>
                    <div class="teacher-form-checkbox">
                        <label class="teacher-form-label" for="female">Female</label>
                        <input class="teacher-form-input" type="radio" id="female" name="sex">
                    </div>
                </div>
                <div class="teacher-form-item teacher-form-item_halfSize teacher-form-item_alone">
                    <label class="teacher-form-label label-for-inline label-for-color"  for="color">Background color</label>
                    <input class="teacher-form-input" type="color" id="color">
                </div>
                <div class="teacher-form-item">
                    <label class="teacher-form-label" for="notes">Notes (optional)</label>
                    <textarea class="teacher-form-input" id="notes" rows="5"></textarea>
                </div>
                <button class="teacher-form-button" type="button" id="addTeacherButton">Add</button>
            </form>
        </div>
    </div>
    `;

    return markup;
}

function closeAddTeacherDialog() {
    addTeacherDialog.close();
    closeAddBtn.removeEventListener('click', closeAddTeacherDialog, false);
}

function addTeacher() {
    const name = document.getElementById("name").value;
    const speciality = document.getElementById("speciality").value;
    const country = document.getElementById("country").value;
    const city = document.getElementById("city").value;
    const email = document.getElementById("email").value;
    const phone = document.getElementById("phone").value;
    const dateOfBirth = document.getElementById("date").value;

    const maleRadio = document.getElementById("male");
    const femaleRadio = document.getElementById("female");

    let gender = "male";

    if (maleRadio.checked) {
        gender = "male";
    } else if (femaleRadio.checked) {
        gender = "female";
    }

    const color = document.getElementById("color").value;
    const notes = document.getElementById("notes").value;

    const newTeacher = {
        full_name: name,
        course: speciality,
        country: country,
        city: city,
        email: email,
        phone: phone,
        b_day: dateOfBirth,
        gender: gender,
        color: color,
        note: notes,
        age: calculateAge(dateOfBirth)
    };

    if (validate(newTeacher)) {
        randomUsers.push(newTeacher);
        createTeacherList(randomUsers);
        
        teacherIcons = document.querySelectorAll('.teacher-icon');
        teacherIcons.forEach((icon, index) => {
            icon.addEventListener('click', () => {
                openTeacherInfoPopup(index, randomUsers);
            });
        });
        
        updateFilterOptions(randomUsers);
        
        tableTeachers = randomUsers;
        createStatsTable(tableTeachers);
        renderTeachersInTable(tableTeachers, currentPage);
        updatePaginationButtons();
        
        console.log("User added and validated");
    } else {
        alert("Enter valid information.");
    }

    addTeacherDialog.close();
}


//----------------------------------------------Initialise website----------------------------------------------
let teacherIcons;

function initializeWebsite() {
    if(Array.isArray(randomUsers) && randomUsers.length > 0) {
        teacherDisplay = document.querySelector('.teacher-list');
        favoriteDisplay = document.querySelector('.favorite-teachers');
        teacherStats = document.querySelector('.stats-table');
        
        function initializeTeacherFavorites() {
            for (const teacher of randomUsers) {
                const isFavorite = localStorage.getItem(`favorite_${teacher.full_name}`) === 'true';
                teacher.favorite = isFavorite;
            }
        }
        
        window.addEventListener('load', initializeTeacherFavorites);
        
        createTeacherList(randomUsers);
        
        const favoritedTeachers = filterArray(randomUsers, null, null, null, true);
        createFavoriteList(favoritedTeachers);
        
        teacherIcons = document.querySelectorAll('.teacher-icon');
        
        teacherIcons.forEach((icon, index) => {
            icon.addEventListener('click', () => {
                openTeacherInfoPopup(index, randomUsers);
            });
        });

        updateFilterOptions(randomUsers);

        tableTeachers = randomUsers;
        createStatsTable(tableTeachers);
        
        renderTeachersInTable(tableTeachers, currentPage);
        updatePaginationButtons();

    } else {
        setTimeout(initializeWebsite, 1000);
    }
}


document.addEventListener("DOMContentLoaded", initializeWebsite);
